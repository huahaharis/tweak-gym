/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native'
import { NativeBaseProvider} from 'native-base'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import Home from './src/screen/Home';
import AddNew from './src/screen/AddNewArea';
import ListPreset from './src/screen/ListPreset';

const {height, width} = Dimensions.get('screen');

const Stack = createStackNavigator();

const App = () => {
  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="AddNew"
            component={AddNew}
            options={{
              headerShown: false,
            }}
          />
           <Stack.Screen
            name="ListPreset"
            component={ListPreset}
            options={{
              headerShown: false,
            }}
          />   
        </Stack.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
  );
};

export default App;
