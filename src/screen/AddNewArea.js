import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Modal,
  TextInput,
} from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
import Button from '../components/Button';
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';
import GetDataPickerService from '../services/GetDataPicker.service';
import { ScrollView } from 'native-base';
import {convertToRupiah} from '../components/converter'
import CreateAreaService from '../services/CreateArea.service';
import GetDataAreaService from '../services/GetDataArea.service';
import Loading from '../components/Loading';


const {height, width} = Dimensions.get('screen');

const AddNew = (props) => {
    const navigation = useNavigation()
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [visible2, setVisible2] = React.useState(false);
    const [visible3, setVisible3] = React.useState(false);
    const [visible4, setVisible4] = React.useState(false);
    const [form, setForm] = React.useState({
      nama: '',
      kapasitas: '',
      harga: ''
    });
    const [dataEdit, setDataEdit] = React.useState({
      presetAreaId: '',
      locationId: '',
      area_activities: [],
      area_equipments: [],
    });
    const [dataActivity, setDataActivity] = React.useState([]);
    const [dataPreset, setDataPreset] = React.useState([]);
    const [dataLokasi, setDataLokasi] = React.useState([]);
    const [dataEquipment, setDataEquipment] = React.useState([]);
    const [dataSelected, setDataSelected] = React.useState([]);
    const [dataSelected2, setDataSelected2] = React.useState([]);
    const [dataSelected3, setDataSelected3] = React.useState([]);
    const [dataSelected4, setDataSelected4] = React.useState([]);

  const hideMenu = () => setVisible(false);

  const showMenu = () => setVisible(true);
  

    React.useEffect(()=>{
      if(props.route.params?.data == undefined ){
      getDataPicker()
      } else {
        setForm({
          nama: props.route.params?.data?.nama,
          kapasitas: props.route.params?.data?.kapasitas.toString(),
          harga: props.route.params.data?.harga?.toString(),
        });
        console.log(JSON.stringify(props.route.params,null,2));

        GetDataPickerService.getDataPresetArea()
        .then(res => {
          let data = [...res.data.data];
          let findValue = data.filter(x => x.id == props.route.params?.data?.presetAreaId);
          console.log(findValue);
          setDataSelected([...findValue])
          setDataPreset(res.data.data);
        })
        .catch(error => {
          console.log(error);
        });

        GetDataPickerService.getDataLokasi()
        .then(res => {
          setDataLokasi(res.data.data);
            let data = [...res.data.data];
            let findValue = data.filter(x => x.id == props.route.params?.data?.locationId);
            setDataSelected2([...findValue]);
            setDataLokasi(res.data.data);
            //dataKe4();
            console.log(res.data.data)
        })
        .catch(err => {
          console.log(err);
        });

        GetDataPickerService.getDataActivity()
        .then(response => {
          let data = [...response.data.data];
          let dataPenampung = []
          props.route.params?.data?.area_activities.map((res, index) => {
            let findValue = data.find(x => x.id == res.activityid);
            dataPenampung.push(findValue)
            // console.log(JSON.stringify(dataPenampung,null,2));
          });
          console.log(JSON.stringify(dataPenampung,null,2));
          setDataSelected3(dataPenampung);
          setDataActivity(response.data.data);
        })
        .catch(error => {
          console.log(error);
        });

        GetDataPickerService.getDataEquipment()
        .then(res => {
          let data = [...res.data.data];
          let dataPenampung = []
          props.route.params?.data?.area_equipments.map((res, index) => {
            let findValue = data.find(x => x.id == res.equipmentid);
            dataPenampung.push(findValue)
            // console.log(JSON.stringify(dataPenampung,null,2));
          });
          setDataSelected4(dataPenampung)
          setDataEquipment(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });

      }
    },[props.route.params?.data || dataEdit])
    

    const getDataPicker = (paramsid) => {
      GetDataPickerService.getDataActivity()
        .then(response => {
          console.log(response);
          setDataActivity(response.data.data);
        })
        .catch(error => {
          console.log(error);
        });
      GetDataPickerService.getDataPresetArea()
        .then(res => {

            setDataPreset(res.data.data);
        })
        .catch(error => {
          console.log(error);
        });
      GetDataPickerService.getDataLokasi()
        .then(res => {
            setDataLokasi(res.data.data);
        })
        .catch(err => {
          console.log(err);
        });
        GetDataPickerService.getDataEquipment()
        .then((res)=>{
          setDataEquipment(res.data.data)
        })
        .catch((err)=>{
          console.log(err);
        })
    }

    const sendData = ( ) => {
      if(props.route?.params?.data?.type === "EDIT"){
        setLoading(true)
        let presetAreaId = dataSelected[0].id
        let locationId = dataSelected2[0].id
        let activitiesId = []
        let equipmentsId = []
        dataSelected3.map((res, idx)=>{
          const dataPenampung = {
            activityId: res.id
          }
          activitiesId.push(dataPenampung)
        })
        dataSelected4.map((item, index)=>{
          const equipment = {
            equipmentId: item.id
          }
          equipmentsId.push(equipment)
        })
        CreateAreaService.updateArea(
          props.route?.params?.data?.id,
          form.nama,
          form.kapasitas,
          form.harga,
          dataSelected[0].id,
          dataSelected2[0].id,
          activitiesId,
          equipmentsId,
        )
          .then(res => {
            if (res.data.message == 'Preset facility updated') {
              setLoading(false);
              navigation.navigate('Home');
            }
            console.log(JSON.stringify(res.data,null,2));
          })
          .catch(err => {
            console.log(err);
          });
      } else {
      setLoading(true)
      let presetAreaId = dataSelected[0].id
      let locationId = dataSelected2[0].id
      let activitiesId = []
      let equipmentsId = []
      dataSelected3.map((res, idx)=>{
        const dataPenampung = {
          activityId: res.id
        }
        activitiesId.push(dataPenampung)
      })
      dataSelected4.map((item, index)=>{
        const equipment = {
          equipmentId: item.id
        }
        equipmentsId.push(equipment)
      })
      CreateAreaService.createArea(
        form.nama,
        form.kapasitas,
        form.harga,
        dataSelected[0].id,
        dataSelected2[0].id,
        activitiesId,
        equipmentsId
      )
      .then((res)=>{
        if(res.data.message == 'Area created succesfully'){
          setLoading(false)
          navigation.navigate('Home')
        }
        // console.log(JSON.stringify(res.data,null,2));
      })
      .catch((err)=>{
        console.log(err);
      })
    }
    }

    const onInputChange = (value, input) => {
      setForm({
        ...form,
        [input]: value,
      });
    };

    const getData = (data, type) => {
      console.log(type);
      if (type == 'preset') {
        setDataSelected([data]);
      } else if (type == 'lokasi') {
        setDataSelected2([data]);
      } else if (type == 'aktifitas') {
        dataSelected3.push(data);
      } else {
        dataSelected4.push(data);
      }
    };

    const deleteData = (data, activity) => {
      if (activity == 'aktifitas') {
        let dlt = [...dataSelected3];
        let value = dlt.findIndex(x => x.activity == data);
        if (value > -1) {
          dlt.splice(value, 1);
          setDataSelected3(dlt);
        }
      } else {
        let dlt = [...dataSelected4];
        let value = dlt.findIndex(x => x.equipment == data);
        if (value > -1) {
          dlt.splice(value, 1);
          setDataSelected4(dlt);
        }
      }
    };

  return (
    <View style={{flex: 1}}>
      <Loading visible={loading} />
      <SafeAreaView style={{backgroundColor: '#9c26b0'}}>
        <StatusBar backgroundColor={'#9c26b0'} barStyle={'light-content'} />
      </SafeAreaView>
      <View
        style={{
          width: '100%',
          height: height * 0.06,
          flexDirection: 'row',
          backgroundColor: '#9c26b0',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesignIcon
            name="left"
            size={22}
            color={'#FFF'}
            style={{
              marginLeft: width * 0.1,
              marginTop: height * 0.02,
              fontWeight: 'bold',
            }}
          />
        </TouchableOpacity>
        {props.route?.params?.data?.type === 'EDIT' ? (
          <View style={{marginRight: width * 0.24, marginTop: height * 0.02,}}>
            <Text style={{fontWeight: 'bold', color: '#FFF', fontSize: 18}}>
              {`Edit Area: ${form.nama}`}
            </Text>
          </View>
        ) : (
          <View style={{marginRight: width * 0.34, marginTop: height * 0.02}}>
            <Text style={{fontWeight: 'bold', color: '#FFF', fontSize: 18}}>
              Buat Area Baru
            </Text>
          </View>
        )}
      </View>
      <View style={{flex: 1}}>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              marginHorizontal: 30,
              marginTop: 40,
            }}>
            <Text style={{bottom: 4, fontSize: 15}}>Nama Area</Text>
            <View style={{borderWidth: 1, borderColor: '#cfd2d4'}}>
              <TextInput
                defaultValue={form.nama}
                onChangeText={value => onInputChange(value, 'nama')}
                placeholder="e.g: Studio Cicendo"
                style={{width: '100%', height: 50, paddingLeft: 10}}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginHorizontal: 30,
              marginTop: 40,
            }}>
            <Text style={{bottom: 4, fontSize: 15}}>Kapasitas Area</Text>
            <View style={{borderWidth: 1, borderColor: '#cfd2d4'}}>
              <TextInput
                defaultValue={form.kapasitas}
                onChangeText={value => onInputChange(value, 'kapasitas')}
                placeholder="e.g: 10 orang"
                style={{width: '100%', height: 50, paddingLeft: 10}}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginHorizontal: 30,
              marginTop: 40,
            }}>
            <Text style={{bottom: 4, fontSize: 15}}>Harga Area</Text>
            <View style={{borderWidth: 1, borderColor: '#cfd2d4'}}>
              <TextInput
                value={form.harga}
                onChangeText={value => onInputChange(value, 'harga')}
                placeholder="e.g: Rp. 60.000"
                style={{width: '100%', height: 50, paddingLeft: 10}}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginTop: 20,
            }}>
            <Text style={{left: 30, fontSize: 15, top: 8}}>Preset Area</Text>
            <View
              style={{
                borderColor: '#cfd2d4',
                borderWidth: 1,
                height: 60,
                width: '85%',
                alignSelf: 'center',
                marginTop: 15,
              }}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  {dataSelected?.map((item, idx) => (
                    <View
                      key={idx}
                      style={{
                        flexDirection: 'row',
                        width: width * 0.4,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        height: 45,
                        marginHorizontal: 5,
                        top: 5,
                      }}>
                      <Text>{item?.tipeArea}</Text>
                    </View>
                  ))}
                </View>
                <View style={{flex: 0.1}}>
                  <Menu
                    visible={visible2}
                    anchor={
                      <>
                        <View style={{flexDirection: 'row', marginLeft: '20%'}}>
                          <TouchableOpacity
                            onPress={() => setVisible2(!visible2)}
                            style={{marginTop: 15}}>
                            <AntDesignIcon
                              name="down"
                              size={22}
                              color={'#cfd2d4'}
                            />
                          </TouchableOpacity>
                        </View>
                      </>
                    }
                    onRequestClose={() => setVisible2(!visible2)}
                    style={{paddingLeft: 10, width: width * 0.88}}>
                    {dataPreset.map((res, index) => (
                      <MenuItem
                        onPress={() => getData(res, 'preset')}
                        key={index}>
                        {res.tipeArea}
                      </MenuItem>
                    ))}
                  </Menu>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginTop: 20,
            }}>
            <Text style={{left: 30, fontSize: 15, top: 8}}>Lokasi</Text>
            <View
              style={{
                borderColor: '#cfd2d4',
                borderWidth: 1,
                height: 60,
                width: '85%',
                alignSelf: 'center',
                marginTop: 15,
              }}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  {dataSelected2.map((item, idx) => (
                    <View
                      key={idx}
                      style={{
                        flexDirection: 'row',
                        width: width * 0.4,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        height: 45,
                        marginHorizontal: 5,
                        top: 5,
                      }}>
                      <Text>{item?.nama}</Text>
                    </View>
                  ))}
                </View>
                <View style={{flex: 0.1}}>
                  <Menu
                    visible={visible3}
                    anchor={
                      <>
                        <View style={{flexDirection: 'row', marginLeft: '20%'}}>
                          <TouchableOpacity
                            onPress={() => setVisible3(!visible3)}
                            style={{marginTop: 15}}>
                            <AntDesignIcon
                              name="down"
                              size={22}
                              color={'#cfd2d4'}
                            />
                          </TouchableOpacity>
                        </View>
                      </>
                    }
                    onRequestClose={() => setVisible3(!visible3)}
                    style={{paddingLeft: 10, width: width * 0.88}}>
                    {dataLokasi.map((res, index) => (
                      <MenuItem
                        onPress={() => getData(res, 'lokasi')}
                        key={index}>
                        {res.nama}
                      </MenuItem>
                    ))}
                  </Menu>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginTop: 20,
            }}>
            <Text style={{left: 30, fontSize: 15, top: 8}}>
              Tentukan Aktifitas
            </Text>
            <View
              style={{
                borderColor: '#cfd2d4',
                borderWidth: 1,
                height: 60,
                width: '85%',
                alignSelf: 'center',
                marginTop: 15,
              }}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <ScrollView style={{height: 30}} horizontal>
                    {dataSelected3.map((item, idx) => (
                      <View
                        key={idx}
                        style={{
                          backgroundColor: '#d1f1eb',
                          flexDirection: 'row',
                          width: width * 0.2,
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          height: 25,
                          marginHorizontal: 5,
                          top: 5,
                        }}>
                        <Text>{item?.activity}</Text>
                        <TouchableOpacity
                          onPress={() =>
                            deleteData(item.activity, 'aktifitas')
                          }>
                          <AntDesignIcon name="close" color={'#148670'} />
                        </TouchableOpacity>
                      </View>
                    ))}
                  </ScrollView>
                </View>
                <View style={{flex: 0.1}}>
                  <Menu
                    visible={visible}
                    anchor={
                      <>
                        <View style={{flexDirection: 'row', marginLeft: '20%'}}>
                          <TouchableOpacity
                            onPress={showMenu}
                            style={{marginTop: 15}}>
                            <AntDesignIcon
                              name="down"
                              size={22}
                              color={'#cfd2d4'}
                            />
                          </TouchableOpacity>
                        </View>
                      </>
                    }
                    onRequestClose={hideMenu}
                    style={{paddingLeft: 10, width: width * 0.88}}>
                    {dataActivity.map((res, index) => (
                      <MenuItem
                        onPress={() => getData(res, 'aktifitas')}
                        key={index}>
                        {res.activity}
                      </MenuItem>
                    ))}
                  </Menu>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginTop: 20,
            }}>
            <Text style={{left: 30, fontSize: 15, top: 8}}>
              Input Equipment
            </Text>
            <View
              style={{
                borderColor: '#cfd2d4',
                borderWidth: 1,
                height: 60,
                width: '85%',
                alignSelf: 'center',
                marginTop: 15,
              }}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <ScrollView style={{height: 30}} horizontal>
                    {dataSelected4.map((item, idx) => (
                      <View
                        key={idx}
                        style={{
                          backgroundColor: '#d1f1eb',
                          flexDirection: 'row',
                          width: width * 0.2,
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          height: 25,
                          marginHorizontal: 5,
                          top: 5,
                        }}>
                        <Text>{item?.equipment}</Text>
                        <TouchableOpacity
                          onPress={() =>
                            deleteData(item.equipment, 'equipment')
                          }>
                          <AntDesignIcon name="close" color={'#148670'} />
                        </TouchableOpacity>
                      </View>
                    ))}
                  </ScrollView>
                </View>
                <View style={{flex: 0.1}}>
                  <Menu
                    visible={visible4}
                    anchor={
                      <>
                        <View style={{flexDirection: 'row', marginLeft: '20%'}}>
                          <TouchableOpacity
                            onPress={() => setVisible4(!visible4)}
                            style={{marginTop: 15}}>
                            <AntDesignIcon
                              name="down"
                              size={22}
                              color={'#cfd2d4'}
                            />
                          </TouchableOpacity>
                        </View>
                      </>
                    }
                    onRequestClose={() => setVisible4(!visible4)}
                    style={{paddingLeft: 10, width: width * 0.88}}>
                    {dataEquipment.map((res, index) => (
                      <MenuItem
                        onPress={() => getData(res, 'equipment')}
                        key={index}>
                        {res.equipment}
                      </MenuItem>
                    ))}
                  </Menu>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
      <Button nama={'SAVE AREA'} route={'Save'} onpress={() => sendData()} />
    </View>
  );
};

export default AddNew;
