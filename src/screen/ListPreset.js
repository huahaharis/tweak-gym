import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import Button from '../components/Button';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
import GetPreset from '../services/GetPreset.service';
import Dialog, {
    ScaleAnimation,
    DialogContent,
  } from 'react-native-popup-dialog';
import {Divider} from 'native-base';
import CreatePreset from '../services/CreatePreset.service';

const {height, width} = Dimensions.get('screen');

const ListPreset = () => {
  const navigation = useNavigation();
  const [dataPreset, setDataPreset] = React.useState([]);
  const [visible, setVisible] = React.useState(false);
  const [inputData, setInputData] = React.useState('');


  React.useEffect(() => {
    getDataPreset();
  }, [visible]);

  const getDataPreset = () => {
    GetPreset.getListPreset()
      .then(response => {
        setDataPreset(response.data.data)
      })
      .catch(err => {
        console.log(err);
      });
  };

  const savePreset = () => {
      CreatePreset.createPreset(inputData)
      .then((response)=>{
          setVisible(false)
      })
      .catch((error)=>{
          console.log(error);
      })
  }

  return (
    <View style={{flex: 1}}>
      <SafeAreaView style={{backgroundColor: '#9c26b0'}}>
        <StatusBar backgroundColor={'#9c26b0'} barStyle={'light-content'} />
      </SafeAreaView>
      <View
        style={{
          width: '100%',
          height: height * 0.06,
          flexDirection: 'row',
          backgroundColor: '#9c26b0',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesignIcon
            name="left"
            size={22}
            color={'#FFF'}
            style={{
              marginLeft: width * 0.1,
              marginTop: height * 0.02,
              fontWeight: 'bold',
            }}
          />
        </TouchableOpacity>
        <View style={{marginRight: width * 0.36, marginTop: height * 0.02}}>
          <Text style={{fontWeight: 'bold', color: '#FFF', fontSize: 18}}>
            Daftar Preset
          </Text>
        </View>
      </View>
      <View style={{flex: 12}}>
        <ScrollView>
          {dataPreset.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={{marginVertical: 10, justifyContent: 'center'}}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{marginLeft: width * 0.12, fontSize: 16}}>
                  {item.tipeArea}
                </Text>
                <TouchableOpacity>
                  <AntDesignIcon
                    name="right"
                    style={{marginRight: width * 0.12}}
                    size={22}
                    color={'black'}
                  />
                </TouchableOpacity>
              </View>
              <Divider
                my={2}
                style={{backgroundColor: '#d9dbda', width: '100%'}}
              />
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
      <TouchableOpacity
        onPress={() => setVisible(true)}
        style={{
          width: '80%',
          height: 40,
          backgroundColor: '#9c26b0',
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          borderRadius: 4,
          marginBottom: height * 0.05,
        }}>
        <Text style={{color: '#FFF', fontWeight: 'bold'}}>TAMBAH PRESET</Text>
      </TouchableOpacity>
      <Dialog
        visible={visible}
        onTouchOutside={() => {
          setVisible(false);
        }}
        dialogAnimation={
          new ScaleAnimation({
            initialValue: 0,
            useNativeDriver: true,
          })
        }>
        <DialogContent
          style={{width: width * 0.7, height: height * 0.3, flex: 0.4}}>
          <View style={{flex: 1, marginTop: '30%'}}>
            <TouchableOpacity
              onPress={() => setVisible(false)}
              style={{alignSelf: 'flex-end', bottom: 55}}>
              <AntDesignIcon name="close" size={20} />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 16,
                color: '#47525E',
                textAlign: 'center',
                marginVertical: 10,
              }}>
              Nama Preset
            </Text>
            <View style={{borderWidth: 1, borderColor: '#cfd2d4'}}>
              <TextInput
                placeholder="e.g: lapangan tenis"
                onChangeText={value => setInputData(value)}
                style={{width: '100%', height: 50, paddingLeft: 10}}
              />
            </View>
            <TouchableOpacity
              onPress={() => savePreset()}
              disabled={inputData === '' ? true : false}
              style={{
                marginTop: 20,
                backgroundColor: inputData === '' ? '#d9cddb' : '#9c26b0',
                height: height * 0.06,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontWeight: 'bold', color: '#FFF'}}>SAVE</Text>
            </TouchableOpacity>
          </View>
        </DialogContent>
      </Dialog>
    </View>
  );
};

export default ListPreset;
