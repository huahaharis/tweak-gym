import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import GetStudioService from '../services/GetStudio.service';
import GetDataPickerService from '../services/GetDataPicker.service';
import {Divider, ScrollView} from 'native-base';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Button from '../components/Button';
import {useNavigation, useIsFocused} from '@react-navigation/native'
import GetDataAreaService from '../services/GetDataArea.service';

const {height, width} = Dimensions.get('screen');

const Home = (props) => {
  const isFocused = useIsFocused()
  const [dataArea, setDataArea] = React.useState([]);
  const [form, setForm] = React.useState({
    nama: '',
    kapasitas: '',
    harga: ''
  });

  const navigation =useNavigation();

    React.useEffect(()=>{
        getDataStudio();
    },[isFocused])

    const getDataStudio =()=>{
        GetStudioService.getListStudio()
        .then((response)=>{
          setDataArea(response.data.data)
        })
        .catch((err)=>{
          console.log(err);
        })
    }

    const getDetailArea = (id) => {
      GetDataAreaService.getDataArea(id)
        .then(res => {
          console.log(JSON.stringify(res.data.data, null, 2));
          setForm({
            nama: res.data.data.nama,
            kapasitas: res.data.data.kapasitas,
            harga: res.data.data.harga,
          });
          
        navigation.navigate('AddNew', {
          data: {
            nama: res.data.data.nama,
            kapasitas: res.data.data.kapasitas,
            harga: res.data.data.harga,
            presetAreaId: res.data.data.presetAreaId,
            area_activities: res.data.data.area_activities,
            locationId: res.data.data.locationId,
            area_equipments: res.data.data.area_equipments,
            type: 'EDIT',
            id: id
          },
        });
        })
        .catch(err => {
          console.log(err);
        });
    
    }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#FFF'}}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          marginTop: 15,
          justifyContent: 'space-between',
        }}>
        <View style={{marginLeft: width * 0.08}}>
          <Text style={{fontWeight: 'bold', fontSize: 17}}>Area Settings</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('ListPreset')}
          style={{
            marginRight: width * 0.08,
            height: height * 0.03,
            width: width * 0.25,
            backgroundColor: '#d4faec',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 6,
            elevation: 10,
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 4},
            shadowOpacity: 0.2,
            shadowRadius: 1,
          }}>
          <Text style={{fontSize: 12}}>Edit Preset</Text>
        </TouchableOpacity>
      </View>
      <View style={{flex: 12}}>
        <ScrollView>
        {dataArea.map((item, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => getDetailArea(item.id)}
            style={{marginVertical: 10, justifyContent: 'center'}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{marginLeft: width * 0.12, fontSize: 16}}>
                {item.nama}
              </Text>
              <TouchableOpacity>
                <FeatherIcon
                  name="settings"
                  style={{marginRight: width * 0.12}}
                  size={22}
                  color={'#bfbebb'}
                />
              </TouchableOpacity>
            </View>
            <Divider
              my={2}
              style={{backgroundColor: '#d9dbda', width: '100%'}}
            />
          </TouchableOpacity>
        ))}
        </ScrollView>
      </View>
      <Button nama={'ADD AREA'} navigation={navigation} route={'AddNew'} />
    </SafeAreaView>
  );
};

export default Home;
