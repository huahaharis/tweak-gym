import React from "react";
import {Modal, View} from 'react-native';
import LottieView from 'lottie-react-native';

const Loading = (props) =>{
    return (
      <Modal
        // onRequestClose={() => setVisible(false)}
        visible={props.visible}
        style={{backgroundColor: 'rgba(52, 52, 52, 0.8)'}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <LottieView
            style={{height: 100, width: 100}}
            source={require('../assets/lf30_editor_k6umawf0.json')}
            autoPlay
            loop
          />
        </View>
      </Modal>
    );
}

export default Loading