import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
const {height, width} = Dimensions.get('screen');

const Button = props => {
  const dinamisButton = () => {
    if (props.route == 'Save') {
      return (
        <TouchableOpacity
          onPress={props.onpress}
          style={{
            width: '80%',
            height: 40,
            backgroundColor: '#9c26b0',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderRadius: 4,
            marginBottom: height * 0.05,
          }}>
          <Text style={{color: '#FFF', fontWeight: 'bold'}}>{props.nama}</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => props.navigation.navigate(props.route)}
          style={{
            width: '80%',
            height: 40,
            backgroundColor: '#9c26b0',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            borderRadius: 4,
            marginBottom: height * 0.05,
          }}>
          <Text style={{color: '#FFF', fontWeight: 'bold'}}>{props.nama}</Text>
        </TouchableOpacity>
      );
    }
  };

  return <View>{dinamisButton()}</View>;
};

export default Button;
