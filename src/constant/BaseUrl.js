import axios from 'axios'

export default axios.create({
    baseURL: 'https://api-devel.tweakindonesia.id/',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });