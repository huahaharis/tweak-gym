import BaseUrl from '../constant/BaseUrl'

const getDataActivity = () => {
    return BaseUrl.get(
      `activity/get`,
    );
  };

  const getDataPresetArea = () => {
    return BaseUrl.get(
      `presetArea/get`,
    );
  };

  const getDataLokasi = () => {
    return BaseUrl.get(
      `location/get`,
    );
  };

  const getDataEquipment = () => {
    return BaseUrl.get(
      `equipment/get`,
    );
  };
export default {
  getDataActivity,
  getDataEquipment,
  getDataLokasi,
  getDataPresetArea,
};