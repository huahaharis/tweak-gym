import BaseUrl from "../constant/BaseUrl";
    
   const getListStudio = () => {
     return BaseUrl.get(
       `area/get`,
     );
   };

export default {getListStudio};