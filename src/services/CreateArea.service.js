import BaseUrl from "../constant/BaseUrl";
    
   const createArea = (namaArea, kapasArea, hargaArea, presetArea, lokasiArea, activitiess, equimentss) => {
       console.log(activitiess, equimentss);
     return BaseUrl.post(`area/create`, {
       nama: namaArea,
       kapasitas: kapasArea,
       harga: hargaArea,
       status: true,
       presetAreaId: presetArea,
       locationId: lokasiArea,
       activitiesId: activitiess,
       equipmentsId: equimentss
     })
   };

   const updateArea = (id,namaArea, kapasArea, hargaArea, presetArea, lokasiArea, activitiess, equimentss) => {
    console.log(activitiess, equimentss);
  return BaseUrl.put(`area/update/${id}`, {
    nama: namaArea,
    kapasitas: kapasArea,
    harga: hargaArea,
    status: true,
    presetAreaId: presetArea,
    locationId: lokasiArea,
    activitiesId: activitiess,
    equipmentsId: equimentss
  })
};

export default {createArea, updateArea};