import BaseUrl from "../constant/BaseUrl";
    
   const createPreset = (body) => {
     return BaseUrl.post(`presetArea/create`, {tipeArea: body});
   };

export default {createPreset};