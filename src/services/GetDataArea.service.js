import BaseUrl from "../constant/BaseUrl";
    
   const getDataArea = (id) => {
     return BaseUrl.get(`area/show/${id}`);
   };

export default {getDataArea};