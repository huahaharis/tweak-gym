import BaseUrl from "../constant/BaseUrl";
    
   const getListPreset = () => {
     return BaseUrl.get(
       `presetArea/get`,
     );
   };

export default {getListPreset};